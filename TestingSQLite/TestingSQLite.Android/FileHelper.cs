﻿using TestingSQLite.Data;
using System;
using Xamarin.Forms;
using System.IO;
using StoringCollection.Droid;
using System.Threading.Tasks;

[assembly: Dependency(typeof(FileHelper))]
namespace StoringCollection.Droid
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}