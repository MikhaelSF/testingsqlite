﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestingSQLite.Data;

using Xamarin.Forms;

namespace TestingSQLite
{
    public partial class App : Application
    {
        static PersonDatabase _database; 

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new TestingSQLite.Views.EntryPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        // TODO: Completar los acesores de esta propiedad.
        public static PersonDatabase Database
        {
            get
            {
                if (_database == null)
                {
                    var specificPath = DependencyService.Get<IFileHelper>().GetLocalFilePath("PersonDatabase.db3");
                    _database = new PersonDatabase(specificPath);
                }
                return _database;
            }
        }
    }
}
