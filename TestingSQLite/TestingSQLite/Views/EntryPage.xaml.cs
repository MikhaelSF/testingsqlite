﻿using System;
using TestingSQLite.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestingSQLite.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryPage : ContentPage
    {
        EntryPageViewModel vm;
        public EntryPage()
        {
            vm = new EntryPageViewModel();
            InitializeComponent();
            BindingContext = vm;
        }

        private void AddPersonButton_Clicked(object sender, EventArgs e)
        {
            vm.AddToPeople();
            Navigation.PushAsync(new MainPage());
        }
    }
}