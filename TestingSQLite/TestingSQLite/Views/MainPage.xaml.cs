﻿using System;
using TestingSQLite.ViewModels;
using Xamarin.Forms;
using TestingSQLite.Views;

namespace TestingSQLite
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel();
        }

        private void BackToEntryButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EntryPage());
        }
    }
}
