﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using TestingSQLite.Models;
using System.Runtime.CompilerServices;

namespace TestingSQLite.ViewModels
{
    class MainPageViewModel : INotifyPropertyChanged
    {
        // Items to list.
        public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person>();

        // Command for the selected Item.
        public ICommand ItemSelectedCommand { get; private set; }

        // Selected Item Text.
        private string selectedItemText { get; set; }
        public string SelectedItemText
        {
            get
            {
                return selectedItemText;
            }
            set
            {
                selectedItemText = value;
                RaisePropertyChange();
            }
        }

        public MainPageViewModel()
        {
            // Adding people to the list.
            Person person = new Person()
            {
                FirstName = "Mikhael",
                LastName = "Santos Fernández",
                PhoneNumber = "(849) 351-8051"
            };

            People.Add(person);

            populatePeople();

            ItemSelectedCommand = new Command<Person>(HandleItemSelected);
        }

        private async void populatePeople()
        {
            List<Person> people = await App.Database.GetPeopleAsync();
            foreach (var person in people)
            {
                People.Add(person);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChange([CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        private void HandleItemSelected(Person person) // Gestionar el elemento seleccionado.
        {
            SelectedItemText = $"{person.FirstName} {person.LastName}";
        }
    }
}
