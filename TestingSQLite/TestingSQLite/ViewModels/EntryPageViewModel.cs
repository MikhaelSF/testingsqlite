﻿using TestingSQLite.Models;

namespace TestingSQLite.ViewModels
{
    class EntryPageViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public EntryPageViewModel()
        { }
        
        public void AddToPeople()
        {
            Person person = new Person()
            {
                FirstName = FirstName,
                LastName = LastName,
                PhoneNumber = PhoneNumber
            };

            App.Database.SavePersonAsync(person);
        }
    }
}
