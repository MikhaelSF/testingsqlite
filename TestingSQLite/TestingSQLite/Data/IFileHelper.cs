﻿using System.Threading.Tasks;

namespace TestingSQLite.Data
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
